-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2015 at 05:49 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `okadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelompoks`
--

CREATE TABLE IF NOT EXISTS `kelompoks` (
`id` int(11) NOT NULL,
  `nama_kelompok` varchar(512) NOT NULL,
  `nama_mapen` varchar(512) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompoks`
--

INSERT INTO `kelompoks` (`id`, `nama_kelompok`, `nama_mapen`) VALUES
(1, 'GUDEG', ''),
(2, 'PEMPEK', ''),
(3, 'SATE MADURA', ''),
(4, 'RUJAK CINGUR', ''),
(5, 'LOTEK', ''),
(6, 'KETOPRAK', ''),
(7, 'TAHU GEJROT', ''),
(8, 'BATAGOR', ''),
(9, 'PAPEDA', ''),
(10, 'AYAM BETUTU', ''),
(11, 'KLAPERTART', ''),
(12, 'IKAN ARSIK', ''),
(13, 'BIKA AMBON', ''),
(14, 'SERABI', ''),
(15, 'JADAH TEMPE', ''),
(16, 'CIRENG', ''),
(17, 'WEDANG RONDE', ''),
(18, 'MENDOAN', ''),
(19, 'BREM', ''),
(20, 'SEMAR MENDEM', ''),
(21, 'BRENEBON', ''),
(22, 'TEMPOYA', ''),
(23, 'SEGO MEGONO', ''),
(24, 'OTAK OTAK', ''),
(25, 'ES DAWET AYU', ''),
(26, 'RAWON', ''),
(27, 'BAKPIA PATHOK', ''),
(28, 'RENDANG', ''),
(29, 'CARICA', ''),
(30, 'ES PISANG IJO', ''),
(31, 'WINGKO BABAT', ''),
(32, 'TAHU SUMEDANG', ''),
(33, 'TEKWAN', ''),
(34, 'BAKWAN KAWI', ''),
(35, 'BANDENG PRESTO', ''),
(36, 'SEGO PECEL MADIUN', ''),
(37, 'WAJIK', ''),
(38, 'BANTAL', ''),
(39, 'NASI TIMLO', '');

-- --------------------------------------------------------

--
-- Table structure for table `pesertas`
--

CREATE TABLE IF NOT EXISTS `pesertas` (
`id` int(11) NOT NULL,
  `nim` varchar(30) DEFAULT NULL,
  `idkelompok` int(2) DEFAULT NULL,
  `nama` varchar(512) NOT NULL,
  `jenis_kelamin` varchar(512) NOT NULL,
  `tempat_lahir` varchar(512) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `golongan_darah` varchar(512) NOT NULL,
  `agama` varchar(512) NOT NULL,
  `nomor_telpon` varchar(512) NOT NULL,
  `nomor_orangtua` varchar(512) NOT NULL,
  `alamat_asal` varchar(512) NOT NULL,
  `alamat_jogja` varchar(512) NOT NULL,
  `asal_sekolah` varchar(512) NOT NULL,
  `kota_asal_sekolah` varchar(512) NOT NULL,
  `idprodi` int(2) NOT NULL,
  `ukuran_almamater` varchar(512) NOT NULL,
  `alergi` varchar(512) DEFAULT NULL,
  `penyakit_diderita` varchar(512) DEFAULT NULL,
  `alergi_makanan` varchar(512) DEFAULT NULL,
  `vegetarian` varchar(512) NOT NULL,
  `tonti` char(5) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prodis`
--

CREATE TABLE IF NOT EXISTS `prodis` (
`id` int(2) NOT NULL,
  `nama_prodi` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodis`
--

INSERT INTO `prodis` (`id`, `nama_prodi`) VALUES
(1, 'Teknik Informatika'),
(2, 'Sistem Informasi'),
(3, 'Bioteknologi'),
(4, 'Teologi'),
(5, 'Desain Produk'),
(6, 'Arsitektur'),
(7, 'Akuntansi'),
(8, 'Manajemen'),
(10, 'Kedokteran');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(512) NOT NULL,
  `role` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`) VALUES
(1, 'adminoka', '$2a$10$qaLSvKvgSnts8a.60ybq8OOPpWBz1NomWj07bIkQWEV7n3tVQEu0.', 'admin', '2015-06-10 22:27:51', '2015-06-10 22:27:51'),
(2, 'useroka1', '$2a$10$JI8mr6c1M2Os3Hln5xK.C.OUo1kbIsNYSzGbsYBkmjRiesid0ayYG', 'user', '2015-06-10 22:29:05', '2015-06-10 22:29:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelompoks`
--
ALTER TABLE `kelompoks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesertas`
--
ALTER TABLE `pesertas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prodis`
--
ALTER TABLE `prodis`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelompoks`
--
ALTER TABLE `kelompoks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `pesertas`
--
ALTER TABLE `pesertas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prodis`
--
ALTER TABLE `prodis`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
