<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Mohon ketik username'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Username sudah dipakai'
            )
        ),
        'passwordlama' => array(
            'checkpasslama' => array(
                'rule' => array('oldPassword'),
                'message' => 'Password tidak sesuai'
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Mohon ketik password lama'
            )

        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Mohon ketik password'
            )
        ),
        'repassword' => array(
            'checkpass' => array(
                'rule'      => array('checkpasswords'),
                'message' => 'Password tidak cocok'
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Mohon ketik ulang password'
            )
                
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin','user')),
                'message' => 'Mohon pilih role',
                'allowEmpty' => false
            )
        )
    );

    // public function beforeSave($options = array()) {
    //     if (isset($this->data[$this->alias]['password'])) {
    //         $passwordHasher = new BlowfishPasswordHasher();
    //         $this->data[$this->alias]['password'] = $passwordHasher->hash(
    //             $this->data[$this->alias]['password']
    //         );
    //     }
    //     return true;
    // }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password']) && !empty($this->data[$this->alias]['password'])) {
            $passwordhash = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordhash->hash($this->data[$this->alias]['password']);

        } 

        return parent::beforeSave();

    }

    public function checkpasswords()     // to check pasword and confirm password
    {  //print_r($this->data['User']['id']);
        if(strcmp($this->data['User']['password'],$this->data['User']['repassword']) == 0 ) 
        {
            return true;
        }
        return false;
    }

    public function oldPassword() {
        if (isset($this->data[$this->alias]['passwordlama']) && !empty($this->data[$this->alias]['passwordlama'])){
            $user = $this->find('first', array('conditions' => array('username' => $this->data[$this->alias]['username'])));
            $passwordhash = Security::hash($this->data[$this->alias]['passwordlama'], 'blowfish', $user['User']['password']);

            if ($passwordhash === $user['User']['password']) {
                return true;
            } else {
                return false;
            }
        }
    }
}

?>