<?php
App::uses('AppModel', 'Model');

class Peserta extends AppModel {
    var $name = 'Peserta';
    var $primaryKey = 'id';
    public $actsAs = array('Containable');

    public $belongsTo = array('Kelompok' => array('classname'=>'Kelompok','foreignKey' => 'idkelompok'),
                              'Prodi'=>array('classname'=>'Prodi','foreignKey'=>'idprodi'));
    
    public $validate = array(
		        
//		'foto' => array(
//			// http://book.cakephp.org/2.0/en/models/data-validation.html#Validation::uploadError
//			'uploadError' => array(
//				'rule' => 'uploadError',
//				'message' => 'Something went wrong with the file upload',
//				'required' => FALSE,
//				'allowEmpty' => TRUE,
//			),
//			// http://book.cakephp.org/2.0/en/models/data-validation.html#Validation::mimeType
//			'mimeType' => array(
//				'rule' => array('mimeType', array('image/gif','image/png','image/jpg','image/jpeg')),
//				'message' => 'Invalid file, only images allowed',
//				'required' => FALSE,
//				'allowEmpty' => TRUE,
//			),
//			// custom callback to deal with the file upload
//			'processUpload' => array(
//				'rule' => 'processUpload',
//				'message' => 'Something went wrong processing your file',
//				'required' => FALSE,
//				'allowEmpty' => TRUE,
//				'last' => TRUE,
//			)
//		),
        'nim'=>array(
            'numeric'=>array(
                'rule'=>'numeric',
                'message'=>'Pastikan nomor induk hanya berupa angka'
                ),
            'minimal'=>array(
                'rule'=>array('minLength','8'),
                'message'=>'Nomor induk terdiri dari 8 angka'
                ),
            'unique'=>array(
                'rule'=>'isUnique',
                'message'=>'Sudah ada yang menggunakan nomor induk tersebut'
                ),
            'notEmpty'=>array(
                'rule'=>'notEmpty',
                'message'=>'Data harus diisi'
                )
        ),
        'nama'=>array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'jenis_kelamin' => array(
            'rule'=>'notEmpty',
            'message'=>'Mohon pilih salah satu'
        ),
        'tempat_lahir' => array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'agama' => array(
            'rule'=>'notEmpty',
            'message'=>'Mohon pilih salah satu'
        ),
        'nomor_telpon' => array(
            'rule' => 'numeric',
            'message' => 'Pastikan nomor telepon hanya berupa angka'
        ),
        'nomor_orangtua' => array(
            'rule' => 'numeric',
            'message' => 'Pastikan nomor telepon hanya berupa angka'
        ),
        'alamat_asal' => array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'asal_sekolah' => array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'kota_asal_sekolah' => array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'idprodi' => array(
            'rule'=>'notEmpty',
            'message'=>'Mohon pilih salah satu'
        ),
        'vegetarian' => array(
            'rule'=>'notEmpty',
            'message'=>'Data harus diisi'
        ),
        'ukuran_almamater' => array(
            'rule'=>'notEmpty',
            'message'=>'Mohon pilih salah satu'
        ),
        'tonti' => array(
            'rule'=>'notEmpty',
            'message'=>'Mohon pilih salah satu'
        )
    );
    
//    /**
//     * Upload Directory relative to WWW_ROOT
//     * @param string
//     */
//    public $uploadDir = 'uploads';
//
//    /**
//     * Process the Upload
//     * @param array $check
//     * @return boolean
//     */
//    public function processUpload($check=array()) {
//        // deal with uploaded file
//        if (!empty($check['filename']['tmp_name'])) {
//
//            // check file is uploaded
//            if (!is_uploaded_file($check['filename']['tmp_name'])) {
//                return FALSE;
//            }
//
//            // build full filename
//            $filename = WWW_ROOT . $this->uploadDir . DS . Inflector::slug(pathinfo($check['filename']['name'], PATHINFO_FILENAME)).'.'.pathinfo($check['filename']['name'], PATHINFO_EXTENSION);
//
//            // @todo check for duplicate filename
//
//            // try moving file
//            if (!move_uploaded_file($check['filename']['tmp_name'], $filename)) {
//                return FALSE;
//
//            // file successfully uploaded
//            } else {
//                // save the file path relative from WWW_ROOT e.g. uploads/example_filename.jpg
//                $this->data[$this->alias]['filepath'] = str_replace(DS, "/", str_replace(WWW_ROOT, "", $filename) );
//            }
//        }
//
//        return TRUE;
//    }
//
//    /**
//     * Before Save Callback
//     * @param array $options
//     * @return boolean
//     */
//    public function beforeSave($options = array()) {
//        // a file has been uploaded so grab the filepath
//        if (!empty($this->data[$this->alias]['filepath'])) {
//            $this->data[$this->alias]['filename'] = $this->data[$this->alias]['filepath'];
//        }
//
//        return parent::beforeSave($options);
//    }
}
?>