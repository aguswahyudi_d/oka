<?php
    App::uses('AppController', 'Controller');

	class KelompoksController extends AppController{
		public $components = array('Paginator');
        public $theme = "temautama";
        public $layout = "layout1";
        public $uses = array('Peserta','Kelompok','Prodi');

        public function index(){
        	$this->set('title','Daftar Kelompok OKA 2015');
        	$this->paginate = array(
                'limit' => 20
            );
            
            $this->set('kelompoks', $this->paginate('Kelompok'));
        }

        public function add(){
        	 $this->set('title','Tambah Data Kelompok OKA 2015');
            if ($this->request->is('post') || $this->request->is('put')) {
            	$this->Kelompok->create();
                if ($this->Kelompok->save($this->request->data)) {
                    //debug($this->request->data);
                        $this->Session->setFlash(__('Kelompok berhasil ditambahkan.'), 'default', array('class' => 'success'));
                        return $this->redirect(array('controller'=>'kelompoks','action' => 'add'));
                    }              
                $this->Session->setFlash(__('Kelompok tidak berhasil ditambahkan.'));
                return $this->redirect(array('controller'=>'kelompoks','action' => 'add'));
            }
        }

        public function edit($id=null){
            $this->set('title','Edit Data Kelompok OKA 2015');
        	$this->Kelompok->id=$id;
        	if (!$this->Kelompok->exists()) {
                throw new NotFoundException(__('Kelompok tidak valid'));
            }
            if ($this->request->is('post')) { 
            	 if ($this->Kelompok->save($this->request->data)) {
                    $this->Session->setFlash(__('Kelompok berhasil diubah.'), 'default', array('class' => 'success'));
                    return $this->redirect(array('controller'=>'Kelompoks','action' => 'index'));
                }              
                $this->Session->setFlash(__('Kelompok tidak berhasil diubah.'));
            	return $this->redirect(array('controller'=>'Kelompoks','action' => 'index'));
            }
            else {
            	if($id)
				{	
					//TRY CATCH --------------------------------------
					try
					{
						$data = $this->Kelompok->read(null, $id);
						//supaya saat edit ada teks sebelumnya
						$this->request->data = $data;	
					}
					catch (NotFoundException $ex)
					{
						//NOTIFIKASI GAGAL
						$this->Session->setFlash('Permintaan tidak valid');
						$this->redirect(array('controller'=>'Kelompoks','action'=>'index'));	
					}
					//------------------------------------------------
				}
				//jika tdk post atau data tdk terisi, cek ada tidak data parameter
				else
				{
					//jk tdk ada data parameter kasi NOTIFIKASI GAGAL
					$this->Session->setFlash('Tidak ada parameter');
					$this->redirect(array('controller'=>'Kelompoks','action'=>'index'));
				} 
            }

        }

        public function delete($id=null){
        	$this->request->onlyAllow('post');
            $this->Kelompok->id = $id;
            
            if (!$this->Kelompok->exists()) {
                throw new NotFoundException(__('Kelompok tidak valid'));
            }
            
            if ($this->Kelompok->delete()) {
                $this->Session->setFlash('Kelompok telah dihapus.');
                return $this->redirect(array('controller'=>'Kelompoks','action' => 'index')); 
            }
            
            $this->Session->setFlash('Kelompok tidak berhasil dihapus.');
            return $this->redirect(array('controller'=>'Kelompoks','action' => 'index')); 
        }

        public function detail($id=null) {
            $this->set('title','Detail Kelompok OKA 2015');
            $data = $this->Kelompok->Find('first',array('conditions'=>array('Kelompok.id'=> $id)));
            $anggotas = $this->Peserta->Find('all',array('conditions'=>array('Peserta.idkelompok'=>$id),'contain'=>array('Prodi')));
            $this->set(compact('data','anggotas'));
        }

        public function hitungAnggota($id=null){
            $anggotas = $this->Peserta->Find('all',array('conditions'=>array('Peserta.idkelompok'=>$id)));
            $jumlah=array('laki-laki'=>'0', 'perempuan'=>'0');
            foreach ($anggotas as $anggota):
                if(strtolower($anggota['Peserta']['jenis_kelamin'])==='laki-laki'){
                    $jumlah['laki-laki'] += 1;
                }
                else if(strtolower($anggota['Peserta']['jenis_kelamin'])==='perempuan'){
                    $jumlah['perempuan'] += 1;
                }
            
            endforeach;
            unset($anggotas);
            return $jumlah;
        }

        public function search(){
            $this->set('title','Hasil Pencarian Kelompok');
            if (!empty($this->data)) {
                $searchstr = $this->data['Kelompok']['search']; 
                $this->set('searchstring', $this->data['Kelompok']['search']); 
                $this->paginate = array(
                    'limit'=> 20,
                    'conditions' => array( 
                        'or' => array(  
                            $this->data['Kelompok']['dasar']." LIKE" => "%$searchstr%"   
                        ) 
                    ) 
                ); 
                $this->set('kelompoks', $this->paginate('Kelompok')); 
            }
        }
	}
?>