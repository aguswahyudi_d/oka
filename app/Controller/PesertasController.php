<?php
    App::uses('AppController', 'Controller');

    class PesertasController extends AppController{
        public $components = array('Paginator');
        public $theme = "temautama";
        public $layout = "layout1";
        public $uses = array('Peserta','Kelompok','Prodi');
        
        public function index() {
            $this->set('title','Daftar Peserta OKA 2015');
            $this->paginate = array(
                'limit' => 50,
                'contain'=> array('Prodi')
            );
            
            $this->set('pesertas', $this->paginate('Peserta'));
        }
        
        public function add() {
            $this->set('title','Tambah Data Peserta OKA 2015');
            $dataprodi = $this->Prodi->Find('list',array('order'=>'nama_prodi','fields'=>array('Prodi.nama_prodi')));
            $this->set(compact('dataprodi'));

            if ($this->request->is('post')) {
                $this->Peserta->create();
                if ($this->Peserta->save($this->request->data)) {
                    $this->Session->setFlash(__('Peserta berhasil didaftarkan.'), 'default', array('class' => 'success'));
                    return $this->redirect(array('controller'=>'Pesertas','action' => 'add'));
                }
                $this->Session->setFlash(
                    __('Peserta tidak berhasil didaftarkan.')
                );
            }

            // if ($this->request->is('post')) {
            //     if (!empty($this->request->data['Peserta']['foto']['tmp_name']) &&
            //        is_uploaded_file($this->request->data['Peserta']['foto']['tmp_name'])) {
            //         $file = $this->request->data['Peserta']['foto'];

            //         $ext = substr(strtolower(strchr($file['name'], '.')),1);
            //         $arr_ext = array('jpg', 'jpeg', 'gif', 'png');

            //         if (in_array($ext, $arr_ext)) {
            //             move_uploaded_file($file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/oka/app/webroot/fotopeserta/' . $file['name']);

            //             $this->request->data['Peserta']['foto'] = $file['name'];
            //             $this->Peserta->create();                        
            //         }
            //     } 
            //     else {
            //         $this->request->data['Peserta']['foto'] = null;
            //         $this->Peserta->create();                        
            //     }
            //     $this->Peserta->set($this->request->data);
            //     $this->Peserta->validates();
            //     if ($this->Peserta->save($this->request->data)) {
            //             $this->Session->setFlash(__('Peserta berhasil didaftarkan.'), 'default', array('class' => 'success'));
            //             return $this->redirect(array('controller'=>'pesertas','action' => 'add'));
            //         }              
            //     $this->Session->setFlash(__('Peserta tidak berhasil didaftarkan.'));
            //     return $this->redirect(array('controller'=>'pesertas','action' => 'add'));
            // }
        }
        
        public function edit($id=null) {
            $this->set('title','Edit Peserta OKA');
            $this->Peserta->id = $id;
            
            if ($this->request->is('post')) {  
                if ($this->Peserta->save($this->request->data)) {
                    $this->Session->setFlash(__('Peserta berhasil diedit.'), 'default', array('class' => 'success'));
                    return $this->redirect(array('controller'=>'Pesertas','action' => 'edit',$id));
                }
                $this->Session->setFlash(
                    __('Peserta tidak berhasil diedit.')
                );             
                // if (!empty($this->request->data['Peserta']['foto']['tmp_name']) &&
                //    is_uploaded_file($this->request->data['Peserta']['foto']['tmp_name'])) {
                //     $file = $this->request->data['Peserta']['foto'];

                //     $ext = substr(strtolower(strchr($file['name'], '.')),1);
                //     $arr_ext = array('jpg', 'jpeg', 'gif', 'png');

                //     if (in_array($ext, $arr_ext)) {
                //         move_uploaded_file($file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/oka/app/webroot/fotopeserta/' . $file['name']);

                //         $this->request->data['Peserta']['foto'] = $file['name'];
                //         $this->Peserta->create();                        
                //     }
                // } 
                // else {
                //     $this->request->data['Peserta']['foto'] = null;                        
                // }
                // if ($this->Peserta->save($this->request->data)) {
                //     $this->Session->setFlash(__('Peserta berhasil diubah.'), 'default', array('class' => 'success'));
                //     return $this->redirect(array('action' => 'index'));
                // }              
                // $this->Session->setFlash(__('Peserta tidak berhasil diubah.'));
            }
            else {
                if($id)
                {   
                    //TRY CATCH --------------------------------------
                    try
                    {
                        $dataprodi = $this->Prodi->Find('list',array('order'=>'nama_prodi','fields'=>array('Prodi.nama_prodi')));
                        $datakelompok = $this->Kelompok->Find('list',array('order'=>'nama_kelompok','fields'=>array('Kelompok.nama_kelompok')));
                        $data = $this->Peserta->read(null, $id);
                        //supaya saat edit ada teks sebelumnya
                        $this->request->data = $data;   
                        $this->set(compact('dataprodi','datakelompok'));
                    }
                    catch (NotFoundException $ex)
                    {
                        //NOTIFIKASI GAGAL
                        $this->Session->setFlash('Permintaan tidak valid');
                        $this->redirect(array('controller'=>'Pesertas','action'=>'index'));    
                    }
                    //------------------------------------------------
                }
                //jika tdk post atau data tdk terisi, cek ada tidak data parameter
                else
                {
                    //jk tdk ada data parameter kasi NOTIFIKASI GAGAL
                    $this->Session->setFlash('Permintaan tidak valid');
                    $this->redirect(array('controller'=>'Pesertas','action'=>'index'));
                } 
            }
            
        }
        
        public function delete($id=null) {
            // $this->request->onlyAllow('post');
            $this->Peserta->id = $id;
            
            if (!$this->Peserta->exists()) {
                throw new NotFoundException(__('Peserta tidak valid'));
            }
            if ($this->request->is('post')) { 
            if ($this->Peserta->delete()) {
                $this->Session->setFlash('Peserta telah dihapus.');
                return $this->redirect(array('action' => 'index')); 
            }}
            
            $this->Session->setFlash('Peserta tidak berhasil dihapus.');
            return $this->redirect(array('controller'=>'Pesertas','action' => 'index'));         
        }
        
        public function detail($id=null) {
            $this->set('title','Data Peserta OKA 2015');
            $data = $this->Peserta->Find('first',array('conditions'=>array('Peserta.id'=> $id),'contain'=> array('Prodi','Kelompok')));
            //$data = $this->Peserta->findById($id);
            $this->set(compact('data'));
        }

        public function download($id=null){
            set_time_limit (0);
            $data = $this->Peserta->find('all',array('contain'=>array('Prodi','Kelompok')));
            $this->set(compact('data'));
        }

        public function hitungAnggota(){
            $anggotas = $this->Peserta->Find('all');
            $jumlah=array('laki-laki'=>'0', 'perempuan'=>'0');
            foreach ($anggotas as $anggota):
                if(strtolower($anggota['Peserta']['jenis_kelamin'])==='laki-laki'){
                    $jumlah['laki-laki'] += 1;
                }
                else if(strtolower($anggota['Peserta']['jenis_kelamin'])==='perempuan'){
                    $jumlah['perempuan'] += 1;
                }
            
            endforeach;
            unset($anggotas);
            return $jumlah;
        }

        public function search(){
            $this->set('title','Hasil Pencarian Peserta');
            if (!empty($this->data)) {
                $searchstr = $this->data['Peserta']['search']; 
                $this->set('searchstring', $this->data['Peserta']['search']); 
                $this->paginate = array(
                    'limit'=> 50,
                    'contain'=> array('Prodi'),
                    'conditions' => array( 
                        'or' => array(  
                            $this->data['Peserta']['dasar']." LIKE" => "%$searchstr%"   
                        ) 
                    ) 
                ); 
                $this->set('pesertas', $this->paginate('Peserta')); 
            }
        }

        public function beforeFilter() {
            parent::beforeFilter();
            $this->Auth->allow('add');
        }

        public function isAuthorized($user=null) {
            // All registered users can add posts
            if (isset($user['role']) && $user['role'] === 'user') {
                if ($this->action === 'index' || $this->action === 'detail' || $this->action === 'download' || $this->action === 'edit' || $this->action === 'search' || $this->action === 'delete' || $this->action === 'add'){
                    return true;
                }
            }
            return parent::isAuthorized($user);
        }
    }
?>