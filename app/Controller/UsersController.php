<?php
	
App::uses('AppController', 'Controller');

class UsersController extends AppController {
	public $components = array('Paginator');
    public $theme = "temautama";
    public $layout = "layout1";
    public $uses = array('User');

    public function login() {
    	$this->set('title','Halaman Login');
	    if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	            return $this->redirect(array('controller'=>'Pesertas','action'=>'index'));
	        }
	        $this->Session->setFlash(__('Username atau password tidak cocok. Mohon coba lagi.'));
	    }
	}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}

    public function index() {
        $this->set('title','Daftar User');
            $this->paginate = array(
                'limit' => 10
            );
            
            $this->set('admins', $this->paginate('User'));
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    public function add() {
    	$this->set('title','Tambah User');
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('User berhasil disimpan.'), 'default', array('class' => 'success'));
                return $this->redirect(array('controller'=>'Users','action' => 'index'));
            }
            $this->Session->setFlash(__('User tidak berhasil disimpan. Mohon coba lagi.'));
        }
    }

    public function edit($id = null) {
        $this->set('title','Edit User');
        $this->User->id=$id;

            if ($this->request->is('post')) { 
                 if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('User berhasil diubah.'), 'default', array('class' => 'success'));
                    return $this->redirect(array('controller'=>'Users','action' => 'index'));
                }              
                $this->Session->setFlash(__('User tidak berhasil diubah.'));
                return $this->redirect(array('controller'=>'Users','action' => 'edit',$id));
            }
            else {
                if($id)
                {   
                    //TRY CATCH --------------------------------------
                    try
                    {
                        $data = $this->User->read(null, $id);
                        //supaya saat edit ada teks sebelumnya
                        $this->request->data = $data;   
                    }
                    catch (NotFoundException $ex)
                    {
                        //NOTIFIKASI GAGAL
                        $this->Session->setFlash('Permintaan tidak valid');
                        $this->redirect(array('controller'=>'Users','action'=>'index'));    
                    }
                    //------------------------------------------------
                }
                //jika tdk post atau data tdk terisi, cek ada tidak data parameter
                else
                {
                    //jk tdk ada data parameter kasi NOTIFIKASI GAGAL
                    $this->Session->setFlash('User tidak ada');
                    $this->redirect(array('controller'=>'Users','action'=>'index'));
                } 
            }
        // $this->User->id = $id;
        // if (!$this->User->exists()) {
        //     throw new NotFoundException(__('User tidak ada'));
        // }
        // if ($this->request->is('post') || $this->request->is('put')) {
        //     if ($this->User->save($this->request->data)) {
        //         $this->Session->setFlash(__('Update user berhasil disimpan.'), 'default', array('class' => 'success'));
        //         return $this->redirect(array('controller'=>'Users','action' => 'index'));
        //     }
        //     $this->Session->setFlash(
        //         __('Update user tidak berhasil disimpan. Mohon coba lagi.')
        //     );
        //     return $this->redirect(array('controller'=>'Users','action' => 'edit',$id));
        // } else {
        //     $this->request->data = $this->User->read(null, $id);
        //     unset($this->request->data['User']['password']);
        // }
    }

    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User berhasil dihapus.'));
            return $this->redirect(array('controller'=>'Users','action' => 'index'));
        }
        $this->Session->setFlash(__('User tidak berhasil dihapus'));
        return $this->redirect(array('controller'=>'Users','action' => 'index'));
    }

    public function search(){
        $this->set('title','Hasil Pencarian User');
            if (!empty($this->data)) {
                $searchstr = $this->data['User']['search']; 
                $this->set('searchstring', $this->data['User']['search']); 
                $this->paginate = array(
                    'limit'=> 10,
                    'conditions' => array( 
                        'or' => array(  
                            $this->data['User']['dasar']." LIKE" => "%$searchstr%"   
                        ) 
                    ) 
                ); 
                $this->set('admins', $this->paginate('User')); 
            }
    }

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to login and logout.
        $this->Auth->allow('login', 'logout');
    }

    public function isAuthorized($user=null) {
            // Only admin can change users data
        if (isset($user['role']) && $user['role'] === 'user') {
            if ($this->action === 'index' || $this->action === 'edit' || $this->action === 'search' || $this->action === 'delete' || $this->action === 'add') {
                return $this->redirect(array('controller'=>'Pesertas','action' => 'index'));
            }
        }
    
        return parent::isAuthorized($user);
    }

}

?>