<style type="text/css">
    #tombol{
        float:left;
        margin: -10px 0px 20px 0px;
    }

    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }
</style>

<table>
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar Prodi UKDW</h2></td>
        <td style="border-bottom:none;">
             <?php echo $this->Form->create('Prodi',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('Prodi.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('Prodi.dasar',array('label' => false,'class'=>'form-control','options'=>array('Prodi.nama_prodi'=>'Nama Prodi'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('Cari',array('class'=>'btn btn-default')); ?>
            </td>
        </td>
    </tr>
</table>

<div id="tombol">
    <button class="btn btn-primary" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Prodis','action'=>'add'))?>'">Tambah data prodi</button>
</div>

<?php
	if(!empty($prodis)){
?>
<table class="table table-bordered table-hover table-condensed">
	<tr>
		<th><?php echo $this->Paginator->sort('id','ID'); ?></th>
		<th><?php echo $this->Paginator->sort('nama_prodi','Nama Prodi'); ?></th>
        <th>Jumlah Mahasiswa (L/P)</th>
		<th>Actions</th>
	</tr>
	<?php foreach($prodis as $prodi): ?>
	<tr>
		<td><?php  echo $prodi['Prodi']['id']; ?></td>
		<td><?php  echo $prodi['Prodi']['nama_prodi']; ?></td>
        <td>
            <?php 
                $jumlah = $this->requestAction('/Prodis/hitungAnggota/'.$prodi['Prodi']['id']); 
                echo $jumlah['laki-laki'].' / '.$jumlah['perempuan'];
            ?>
        </td>
		<td>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Prodis','action'=>'edit', $prodi['Prodi']['id']))?>'">Edit</button>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Prodis','action'=>'delete', $prodi['Prodi']['id']),array('confirm' => 'Apakah anda yakin akan menghapus data ini?'))?>'">Delete</button>
		</td>
	</tr>
	<?php
		endforeach;
		unset($prodis);
	 ?>
</table>

<div>
    <?php 
        echo $this->Paginator->first('First');
        echo " ";    
        
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev('<<');
        }

        echo " ";

        echo $this->Paginator->numbers(array('modulus' => 2));
        echo " ";

        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next('>>');
        }
        echo " ";
    
        echo $this->Paginator->last('Last');
    ?>
</div>

<?php
	}
	else{
		echo "Data prodi tidak ditemukan";
	}
?>