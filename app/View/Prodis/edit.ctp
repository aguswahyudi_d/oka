<style type="text/css">
  h2{
    float:left;
  }
  hr{
    clear: both;
    width: 1080px;
  }
  .tombolkembali{
    float:right;
    position: relative;
    right: 58px;
    top:24px;
  }
  .table-borderless td{ 
      border: 0 !important;
  }
  .table tbody tr{
      background-color: white;
    }
  .table>tbody>tr>td.labelin{
    font-weight: bold;
    text-align: right;
    padding-right: 20px;
    vertical-align: middle;
  }
  button{
    float:right;
  }
</style>

<h2>Edit Data Prodi OKA 2015</h2>
<button class="btn btn-primary tombolkembali" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Prodis','action'=>'index'))?>'">Kembali</button>
<hr>
<?php echo $this->Form->create('Prodi', array('controller'=>'Prodis','action'=>'edit','inputDefaults' => array('class'=>'form-control','label'=>false))); ?>
<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
<table class="table table-hover table-borderless">
  <tbody>
  	<tr><td class="col-md-2 labelin">Nama Prodi</td>
  		<td><?php echo $this->Form->input('nama_prodi',array('div'=>false)); ?></td>
  	</tr>
  </tbody>
</table>
<?php echo $this->Form->button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;&nbsp;Simpan', array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>