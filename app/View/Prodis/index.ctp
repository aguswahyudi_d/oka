<style type="text/css">
    #tombol{
        float:right;
        margin: 0px 0px 10px 0px;
    }

    .header{
        margin-top: -10px;
        margin-bottom: 0px;
    }

    hr{
        margin-top: 0px;
        margin-bottom: 10px;
    }

    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }

    .keterangan{
        font-style: italic;
        font-size: 13pt;
        clear:both;
        text-align: center;
        padding-top: 40px;
    }

    nav{
        text-align: center;
    }
    
</style>

<table class="header">
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar Prodi UKDW</h2></td>
        <td style="border-bottom:none;">
             <?php echo $this->Form->create('Prodi',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('Prodi.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('Prodi.dasar',array('label' => false,'class'=>'form-control','options'=>array('Prodi.nama_prodi'=>'Nama Prodi'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;&nbsp;Cari',array('class'=>'btn btn-default')); ?>
                <?php echo $this->Form->end(); ?>
            </td>
        </td>
    </tr>
</table>
<hr/>

<div id="tombol">
    <?php
        echo $this->Html->link($this->Html->tag('span','', array('class' => 'glyphicon glyphicon-plus','aria-hidden'=>'true')).
                                 '&nbsp;Tambah prodi', array('controller'=>'Prodis', 'action' => 'add'),
                                                  array('class' => 'btn btn-primary', 'escape'=>false)
                                );
    ?>
</div>

<?php
	if(!empty($prodis)){
?>
<table class="table table-bordered table-hover table-condensed">
	<tr>
		<th><?php echo $this->Paginator->sort('id','ID'); ?></th>
		<th><?php echo $this->Paginator->sort('nama_prodi','Nama Prodi'); ?></th>
        <th>Jumlah Mahasiswa (L/P)</th>
		<th>Actions</th>
	</tr>
	<?php foreach($prodis as $prodi): ?>
	<tr>
		<td><?php  echo $prodi['Prodi']['id']; ?></td>
		<td><?php  echo $prodi['Prodi']['nama_prodi']; ?></td>
        <td>
            <?php 
                $jumlah = $this->requestAction('/Prodis/hitungAnggota/'.$prodi['Prodi']['id']); 
                echo $jumlah['laki-laki'].' / '.$jumlah['perempuan'];
            ?>
        </td>
		<td>
            <?php 
                echo $this->Html->link('Edit', 
                    array('controller'=>'Prodis','action'=>'edit',$prodi['Prodi']['id']),
                    array('class'=>'btn btn-default')
                    );
                echo $this->Form->postLink('Delete', 
                    array('controller'=>'Prodis','action'=>'delete',$prodi['Prodi']['id']),
                    array('confirm' => 'Apakah anda yakin akan menghapus data ini?','class'=>'btn btn-default')
                    );
            ?>
		</td>
	</tr>
	<?php
		endforeach;
		unset($prodis);
	 ?>
</table>

<nav>
    <ul class="pagination">
        <?php
            echo $this->Paginator->first(__('First'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            if($this->Paginator->hasPrev()){
                echo $this->Paginator->prev(__('<<'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            }
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            if($this->Paginator->hasNext()){
               echo $this->Paginator->next(__('>>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')); 
            }  
            echo $this->Paginator->last(__('Last'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));              
        ?>
    </ul>
 </nav>

<?php
    }
    else{
        ?>
        <div class="keterangan">
            <?php echo "-- Data program studi belum ada --"; ?>
        </div>
    <?php
    }
?>