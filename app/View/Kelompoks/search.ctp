<style type="text/css">
    #tombol{
        float:left;
        margin: -10px 0px 20px 0px;
    }
    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }
</style>

<table>
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar Kelompok OKA 2015</h2></td>
        <td style="border-bottom:none;">
             <?php echo $this->Form->create('Kelompok',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('Kelompok.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('Kelompok.dasar',array('label' => false,'class'=>'form-control','options'=>array('Kelompok.nama_kelompok'=>'Nama Kelompok','Kelompok.nama_mapen'=>'Nama Mapen'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('Cari',array('class'=>'btn btn-default')); ?>
            </td>
        </td>
    </tr>
</table>

<div id="tombol">
    <button class="btn btn-primary" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Kelompoks','action'=>'add'))?>'">Tambah data kelompok</button>
</div>

<?php
    if(!empty($kelompoks)){
?>
<table class="table table-bordered table-hover table-condensed">
    <tr>
        <th><?php echo $this->Paginator->sort('id','ID'); ?></th>
        <th><?php echo $this->Paginator->sort('nama_kelompok','Nama Kelompok'); ?></th>
        <th><?php echo $this->Paginator->sort('nama_mapen','Mapen'); ?></th>
        <th>Jumlah Anggota (L/P)</th>
        <th>Actions</th>
    </tr>
    <?php foreach($kelompoks as $kelompok): ?>
    <tr>
        <td><?php  echo $kelompok['Kelompok']['id']; ?></td>
        <td><?php echo $this->Html->link($kelompok['Kelompok']['nama_kelompok'], 
                                         array(
                                             'controller' => 'Kelompoks', 
                                             'action' => 'detail', 
                                             $kelompok['Kelompok']['id']
                                         )
                                        ); 
            ?>
        </td>
        <td><?php  echo $kelompok['Kelompok']['nama_mapen']; ?></td>
        <td>
            <?php 
                $jumlah = $this->requestAction('/Kelompoks/hitungAnggota/'.$kelompok['Kelompok']['id']); 
                echo $jumlah['laki-laki'].' / '.$jumlah['perempuan'];
            ?>
        </td>
        <td>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Kelompoks','action'=>'edit', $kelompok['Kelompok']['id']))?>'">Edit</button>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Kelompoks','action'=>'delete', $kelompok['Kelompok']['id']),array('confirm' => 'Apakah anda yakin akan menghapus data ini?'))?>'">Delete</button>
        </td>
    </tr>
    <?php
        endforeach;
        unset($kelompoks);
     ?>
</table>

<div>
    <?php 
        echo $this->Paginator->first('First');
        echo " ";    
        
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev('<<');
        }

        echo " ";

        echo $this->Paginator->numbers(array('modulus' => 2));
        echo " ";

        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next('>>');
        }
        echo " ";
    
        echo $this->Paginator->last('Last');
    ?>
</div>

<?php
    }
    else{
        echo "Data kelompok tidak ditemukan";
    }
?>