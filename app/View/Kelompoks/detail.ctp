<style type="text/css">
	h2{
		float:left;
	}
	hr{
		clear:both;
	}
	.tomboltombol{
		float:right;
		position: relative;
		top:24px;
	}

	#tomboledit{
		margin-right: 5px;
		width: 80px;
	}

	.table-borderless td{ 
	    border: 0 !important;
	}
	.table tbody tr{
	    background-color: white;
	}
	.labelin{
    	font-weight: bold;
    	vertical-align: middle;
  	}
  	.tabeldalam tbody tr td, .tabeldalam tr th{
  		text-align: center;
  		vertical-align: middle;
  	}

</style>
<h2>Detail Kelompok OKA 2015</h2>
<div class="tomboltombol">
	<button class="btn btn-default" id="tomboledit" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Kelompoks','action'=>'edit', $data['Kelompok']['id']))?>'">Edit</button>
	<button class="btn btn-primary" id="tombolkembali" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Kelompoks','action'=>'index'))?>'">Kembali</button>
</div>
<hr>
<table class="table table-borderless">
	<tbody>
		<tr><td class="labelin col-md-2">Nama kelompok</td><td style="width:2%;">:</td><td><?php echo $data['Kelompok']['nama_kelompok'];?></td></tr>
		<tr><td class="labelin">Nama mapen</td><td>:</td><td><?php echo $data['Kelompok']['nama_mapen'];?></td></tr>
		<tr>
			<td class="labelin">Anggota</td><td>:</td>
			<?php if(!empty($anggotas)){ 
				$laki=0;
				$perempuan=0;
				foreach($anggotas as $anggota):
					if(strtolower($anggota['Peserta']['jenis_kelamin'])==='laki-laki'){
						$laki += 1;
					}
					else if(strtolower($anggota['Peserta']['jenis_kelamin'])==='perempuan'){
						$perempuan += 1;
					}
				endforeach;
				?>
				<td>
					<?php echo $laki.' laki-laki, '.$perempuan.' perempuan'; ?>
				</td>
				<table class="table table-bordered tabeldalam">
					<tr>
						<th>ID</th>
						<th>NIM</th>
						<th>Nama</th>
						<th>Prodi</th>
						<th>Jenis Kelamin</th>
						<th>Agama</th>
						<th>Nomor HP</th>
						<th>Penyakit</th>
						<th>Alergi</th>
						<th>Alergi Makanan</th>
						<th>Vegan</th>
					</tr>
					<tbody>
			<?php foreach($anggotas as $anggota): ?>
				<tr>
					<td class="col-md-1"><?php echo $anggota['Peserta']['id']; ?></td>
					<td><?php echo $anggota['Peserta']['nim']; ?></td>
					<td class="col-md-2"><?php echo $anggota['Peserta']['nama']; ?></td>
					<td><?php echo $anggota['Prodi']['nama_prodi']; ?></td>
					<td><?php echo $anggota['Peserta']['jenis_kelamin']; ?></td>
					<td><?php echo $anggota['Peserta']['agama']; ?></td>
					<td><?php echo $anggota['Peserta']['nomor_telpon']; ?></td>
					<td><?php echo $anggota['Peserta']['penyakit_diderita']; ?></td>
					<td><?php echo $anggota['Peserta']['alergi']; ?></td>
					<td><?php echo $anggota['Peserta']['alergi_makanan']; ?></td>
					<td><?php echo $anggota['Peserta']['vegetarian']; ?></td>
				</tr>
			<?php
				endforeach;
	        	unset($anggotas);
		    ?>
		    		</tbody>
		        	</table>
		    <?php }else{ ?>
			<td>Belum ada anggota</td>
			<?php } ?>
		</tr>
	</tbody>
</table>
