<style type="text/css">
	.jedanavbar{
		height: 60px;
	}
</style>

<?php 
	$cek=$this->Session->read('Auth.User.username');
	$role=$this->Session->read('Auth.User.role'); 
?>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		 <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span> 
			</button>
			<a class="navbar-brand" href="#">
				Sistem Pendaftaran OKA
			</a>
	    </div>

		<!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse">
	    	<ul class="nav navbar-nav">
	    		<?php if(strtolower($this->params['controller'])==='pesertas'){ ?>
					<li class="active"><?php echo $this->Html->link('Peserta',array('controller'=>'Pesertas','action'=>'index')); ?></li>
				<?php }else{ ?>
				 	<li><?php echo $this->Html->link('Peserta',array('controller'=>'Pesertas','action'=>'index')); ?></li>
				<?php }
					if(strtolower($this->params['controller'])==='kelompoks'){ ?>
					<li class="active"><?php echo $this->Html->link('Kelompok',array('controller'=>'Kelompoks','action'=>'index')); ?></li>
				<?php }else{ ?>
					<li><?php echo $this->Html->link('Kelompok',array('controller'=>'Kelompoks','action'=>'index')); ?></li>
				<?php } 
					if(strtolower($this->params['controller'])==='prodis'){ ?>
					<li class="active"><?php echo $this->Html->link('Prodi',array('controller'=>'Prodis','action'=>'index')); ?></li>
				<?php }else{ ?>
					<li><?php echo $this->Html->link('Prodi',array('controller'=>'Prodis','action'=>'index')); ?></li>
				<?php } ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php 
					if(strtolower($role)==='admin'){
						if(strtolower($this->params['controller'])==='users'){ ?>
						<li class="active"><?php echo $this->Html->link('Selamat datang, '.$cek,array('controller'=>'Users','action'=>'index')); ?></li>
					<?php }else { ?>
						<li><?php echo $this->Html->link('Selamat datang, '.$cek,array('controller'=>'Users','action'=>'index')); ?></li>
					<?php }
					} else if(strtolower($role)==='user'){ ?>
						<li><p class="navbar-text navbar-right"><?php echo 'Selamat datang, '.$cek; ?></p></li>
				<?php } ?>
				<li>
					<?php
						echo $this->Html->link($this->Html->tag('span','', array('class' => 'glyphicon glyphicon-log-out','aria-hidden'=>'true')).
                                 '&nbsp;Logout', array('controller'=>'Users', 'action' => 'logout'),
                                                  array('escape'=>false)
                                );
					?>
				</li>
			</ul>
	    </div><!-- /.navbar-collapse -->
	</div>
</nav>
<div class="jedanavbar"></div>