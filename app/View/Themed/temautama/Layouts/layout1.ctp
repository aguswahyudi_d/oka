<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--Artinya diatas mendukung berbagai dev-->

        <title>Sistem Pendaftaran OKA</title>
        <?php  
            echo $this->Html->css(array('cake.generic.css','bootstrap.min.css','cssutama.css'));//ada di webroot bro > css
        ?>
    </head>
    <body>
        <div id="wrap">
            <?php //desain headernya di sini ....
                $role=$this->Session->read('Auth.User.role');
                if(strtolower($role)==='admin' || strtolower($role)==='user'){
                    if(strtolower($this->params['controller'])==='pesertas'){
                        if(strtolower($this->params['action'])==='add'){
                            //halaman formulir tanpa navbar
                        }
                        else{
                             echo $this->element('navbar',array('menu'=>strtolower($this->params['controller'])));
                        }
                    }
                    else{
                        echo $this->element('navbar',array('menu'=>strtolower($this->params['controller'])));
                    }   
                }
                else if(empty($role)){
                    //halaman formulir tanpa navbar
                }            
            ?>
            
            <div class="container">
                <?php  
                    echo $this->Session->flash();
                    //cara menempelkan view tadi ke dalam layout 
                    echo $this->fetch('content'); //menunjuk ke objek view (helper), jenis yg akan diambil
                ?>
            </div>
            
            <?php //desain footernya di sini...
            ?>
        </div>
    </body>    
</html>