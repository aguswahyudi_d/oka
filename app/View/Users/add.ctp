<style type="text/css">
	h2{
		float:left;
	}
	hr{
		clear:both;
	}
	.tombolkembali{
		float:right;
		position: relative;
		top:24px;
	}
	.table-borderless td{ 
	    border: 0 !important;
	}
	button{
		float:right;
	}
	.table tbody tr{
    	background-color: white;
    }
	.table>tbody>tr>td.labelin{
	    font-weight: bold;
	    text-align: right;
	    padding-right: 20px;
	    padding-top: 15px;
	}
</style>

<h2>Tambah User</h2>
<button class="btn btn-primary tombolkembali" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Users','action'=>'index'))?>'">Kembali</button>
<hr>
<table>
<?php echo $this->Form->create('User', array('controller'=>'Users','action'=>'add', 'inputDefaults'=>array('class'=>'form-control','label'=>false))); ?>
<table class="table table-hover table-borderless">
	<tbody>
		<tr><td class="col-md-2 labelin">Username</td>
			<td><?php echo $this->Form->input('username',array('div'=>false)); ?></td>
		</tr>
		<tr><td class="labelin">Password</td>
			<td><?php echo $this->Form->input('password',array('div'=>false,'autocomplete'=>'off')); ?></td>
		</tr>
		<tr>
			<td class="labelin">Retype password</td>
			<td><?php echo $this->Form->input('repassword',array('div'=>false,'type'=>'password','autocomplete'=>'off'));?> </td>
		</tr>
		<tr><td class="labelin">Role</td>
			<td><?php echo $this->Form->input('role', array('options' => array('admin' => 'Admin','user' => 'User'))); ?></td>
		</tr>
	</tbody>
</table>
<?php echo $this->Form->button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;&nbsp;Selesai', array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>