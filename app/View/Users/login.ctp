<style type="text/css">
	.panel{
		width:400px;
		position:fixed;
	    top: 15%;
	    left: 35%;
	}

	button{
		float:right;
	}
</style>

<?php echo $this->Session->flash('auth'); ?>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Login Sistem Pendaftaran OKA</h3>
	</div>
	<div class="panel-body">
		<?php echo $this->Form->create('User', array('controller'=>'Users','action'=>'login', 'inputDefaults'=>array('class'=>'form-control'))); ?>
		    <?php echo $this->Form->input('username');
		    echo $this->Form->input('password');
		?>
		<?php echo $this->Form->button('<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>&nbsp;&nbsp;Login', array('class'=>'btn btn-primary')); ?>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

