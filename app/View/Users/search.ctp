<style type="text/css">
    #tombol{
        margin:20px;
    }
    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }
</style>

<table>
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar User</h2></td>
        <td style="border-bottom:none;">
             <?php echo $this->Form->create('User',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('User.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('User.dasar',array('label' => false,'class'=>'form-control','options'=>array('User.username'=>'Username'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('Cari',array('class'=>'btn btn-default')); ?>
            </td>
        </td>
    </tr>
</table>

<div id="tombol">
    <button class="btn btn-primary" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Users','action'=>'add'))?>'">Tambah user</button>
</div>

<?php
    if(!empty($admins)){
?>
<table class="table table-bordered table-hover table-condensed">
    <tr>
        <th><?php echo $this->Paginator->sort('id','ID'); ?></th>
        <th><?php echo $this->Paginator->sort('username','Username'); ?></th>
        <th>Role</th>
        <th>Actions</th>
    </tr>
    <?php foreach($admins as $admin): ?>
    <tr>
        <td><?php  echo $admin['User']['id']; ?></td>
        <td><?php echo $admin['User']['username']; ?></td>
        <td><?php echo $admin['User']['role']; ?></td>
        <td>
            <?php 
            if(strtolower($admin['User']['role'])==='admin'){
                echo '--';
            } else{
            ?>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Users','action'=>'edit', $admin['User']['id']))?>'">Edit</button>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Users','action'=>'delete', $admin['User']['id']),array('confirm' => 'Apakah anda yakin akan menghapus data ini?'))?>'">Delete</button>
            <?php } ?>
        </td>
    </tr>
    <?php
        endforeach;
        unset($admins);
     ?>
</table>

<div>
    <?php 
        echo $this->Paginator->first('First');
        echo " ";    
        
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev('<<');
        }

        echo " ";

        echo $this->Paginator->numbers(array('modulus' => 2));
        echo " ";

        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next('>>');
        }
        echo " ";
    
        echo $this->Paginator->last('Last');
    ?>
</div>

<?php
    }
    else{
        echo "Data user tidak ditemukan";
    }
?>