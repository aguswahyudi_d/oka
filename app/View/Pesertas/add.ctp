<style type="text/css">
  #tombol{
    float: right;
  }
  hr{
    clear: both;
  }
  .form-control{
    display: inline;
  }
  .radio label
  {
    width: 110px;
    padding-left: 0px;
    margin-left: 18px;
    font-size: 11pt;
  }
  .radio input[type=radio]
  {
    margin-left: 0px;
    margin-top: -4px;
  }
  .table-borderless td{ 
      border: 0 !important;
  }
  .table tbody tr{
    background-color: white;
  }
  .table>tbody>tr>td.labelin{
    font-weight: bold;
    text-align: right;
    padding-right: 20px;
    padding-top: 15px;
  }
</style>

<h2>Formulir OKA 2015</h2>
<hr>
<?php echo $this->Form->create('Peserta', array('controller'=>'pesertas','action'=>'add', 'inputDefaults' => array('class'=>'form-control'),'enctype' => 'multipart/form-data')); ?>
<table class="table table-hover table-borderless">
  <tbody>
    <tr><td class="labelin">Nama lengkap</td>
      <td><?php echo $this->Form->input('nama',array('label'=>false,'div'=>false)); ?></td></tr>
    <tr>
        <td class="labelin">Program Studi</td>
        <td>
            <?php echo $this->Form->input('idprodi',array('label'=>false,'div'=>false,
                                                          'options'=>array($dataprodi),'empty'=>'- pilih salah satu -'));?>
        </td>
    </tr>
    <tr>
      <td class="labelin">NIM</td>
      <td>
        <?php echo $this->Form->input('Peserta.nim', array('label'=>false,'div'=>false,'placeholder'=>'nomor induk mahasiswa'));
        ?>
      </td>
    </tr>
    <tr>
        <td class="labelin">Jenis kelamin</td>
        <td>
        <?php echo $this->Form->input('jenis_kelamin',array('legend'=>false, 'type'=>'radio',
                                                            'options'=>array('Perempuan'=>'Perempuan', 'Laki-laki'=>'Laki-laki')));
            ?>
        </td>
    </tr>
    <tr>
        <td class="labelin">Tempat, tanggal lahir</td>
        <td><span class='form-inline'><?php echo $this->Form->input('tempat_lahir',array(
                                                                    'label'=>false,
                                                                    'div'=>false,
                                                                    'style'=>'float:left; width:200px; margin-right:5px;'));
                                            echo $this->Form->input('tanggal_lahir', array(
                                                                 'label'=>false,
                                                                 'div'=>false,
                                                                 'dateFormat' => 'DMY',
                                                                 'minYear' => date('Y') - 70,
                                                                 'maxYear' => date('Y') - 10 ));
            ?></span>
        </td>
    </tr>
    <tr>
        <td class="labelin">Golongan darah</td>
        <td>
            <?php echo $this->Form->input('golongan_darah', array('legend'=>false,'type' => 'radio', 
                                                                  'options' => array(
                                                                                     'A' => 'A', 
                                                                                     'B' => 'B', 
                                                                                     'AB' => 'AB',  
                                                                                     'O' => 'O')));
            ?>
        </td>
    </tr>
    <tr><td class="labelin">Agama</td>
        <td>
            <?php echo $this->Form->input('agama', array('legend'=>false,'type' => 'radio',
                                                                  'options' => array(
                                                                                     'Budha'=>'Budha',
                                                                                     'Hindu' => 'Hindu',
                                                                                     'Islam' => 'Islam',  
                                                                                     'Katholik' => 'Katholik', 
                                                                                     'Khonghucu' => 'Khonghucu',
                                                                                     'Kristen' => 'Kristen'
                                                                                     )));
            ?>
        </td>
    </tr>
    <tr><td class="labelin">Nomor telepon</td>
      <td><?php echo $this->Form->input('nomor_telpon',array('label'=>false,'div'=>false,'placeholder'=>'nomor telepon yang bisa dihubungi')); ?></td></tr>
    <tr><td class="labelin">Nomor orangtua</td><td><?php echo $this->Form->input('nomor_orangtua',array('label'=>false,'div'=>false)); ?> </td></tr>
    <tr><td class="labelin">Alamat asal</td><td><?php echo $this->Form->input('alamat_asal', array('type'=>'textarea','label'=>false,'div'=>false,'placeholder'=>'alamat tinggal di tempat asal')); ?></td></tr>
    <tr><td class="labelin">Alamat di Yogya</td><td><?php echo $this->Form->input('alamat_jogja', array('type'=>'textarea','label'=>false,'div'=>false,'placeholder'=>'alamat tinggal di yogyakarta')); ?></td></tr>
    <tr><td class="labelin">Nama asal SMA/SMK</td><td><?php echo $this->Form->input('asal_sekolah',array('label'=>false,'div'=>false)); ?></td></tr>
    <tr><td class="labelin">Kota asal SMA/SMK</td><td><?php echo $this->Form->input('kota_asal_sekolah',array('label'=>false,'div'=>false)); ?></td></tr>
    <tr><td class="labelin">Jenis Vegetarian</td>
        <td><?php echo $this->Form->input('vegetarian', array('label'=>false, 'div'=>false,'default'=>'Tidak vegetarian')); ?></td>
    </tr><tr><td class="labelin">Alergi makanan</td><td><?php echo $this->Form->input('alergi_makanan', array('default' => 'Tidak ada', 'type'=>'textarea','label'=>false,'div'=>false)); ?></td></tr>
    <tr><td class="labelin">Alergi lain</td><td><?php echo $this->Form->input('alergi', array('default' => 'Tidak ada', 'type'=>'textarea','label'=>false,'div'=>false)); ?></td></tr>
    <tr><td class="labelin">Penyakit</td><td><?php echo $this->Form->input('penyakit_diderita', array('default' => 'Tidak ada','type'=>'textarea','label'=>false,'div'=>false,'placeholder'=>'penyakit yang pernah atau sedang diderita')); ?></td></tr>
    <tr><td class="labelin">Ukuran jas almamater</td>
        <td>
            <?php echo $this->Form->input('ukuran_almamater', array('type' => 'select', 'label'=>false, 'div'=>false,
                                                                    'options' => array('empty'=>'--pilih salah satu--',
                                                                                       'S' => 'S', 
                                                                                       'M' => 'M', 
                                                                                       'L' => 'L', 
                                                                                       'XL' => 'XL', 
                                                                                       'XXL' => 'XXL', 
                                                                                       'XXXL' => 'XXXL',
                                                                                       'XXXXL' => 'XXXXL',
                                                                                       'XXXXXL' => 'XXXXXL'))); 
            ?>
        </td>
    </tr>
    <tr><td class="labelin">Pernah ikut kegiatan baris-berbaris?</td>
        <td>
           <?php echo $this->Form->input('tonti', array('legend'=>false,'type' => 'radio',
                                                                  'options' => array(
                                                                                     'Tidak' => 'Tidak', 
                                                                                     'Ya' => 'Ya')));
            ?>
        </td>
    </tr>
  </tbody>
    
</table>
<div id="tombol">
  <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Pesertas','action'=>'index'))?>'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;&nbsp;Batal</button>
  <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;&nbsp;Selesai', array('class'=>'btn btn-primary')); ?>
  <?php echo $this->Form->end(); ?>
</div>
