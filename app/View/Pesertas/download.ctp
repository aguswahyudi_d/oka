<?php
      // create new empty worksheet and set default font
	$this->PhpExcel->createWorksheet()
	    ->setDefaultFont('Calibri', 12);

	// define table cells
	$table = array(
	    array('label' => __('NIM'), 'filter' => true),
	    array('label' => __('Nama'), 'filter' => true),
	    array('label' => __('Prodi'), 'filter' => true),
	    array('label' => __('Kelompok'), 'filter' => true),
	    array('label' => __('Jenis Kelamin')),
	    array('label' => __('Tempat Lahir'), 'width' => 50, 'wrap' => true),
	    array('label' => __('Tanggal Lahir')),
	    array('label' => __('Gol. Darah')),
	    array('label' => __('Agama')),
	    array('label' => __('Telp')),
	    array('label' => __('Telp Orang Tua')),
	    array('label' => __('Alamat Asal')),
	    array('label' => __('Alamat Yogya')),
	    array('label' => __('Asal Sekolah')),
	    array('label' => __('Kota Asal Sekolah')),
	    array('label' => __('Alergi')),
	    array('label' => __('Alergi Makanan')),
	    array('label' => __('Penyakit')),
	    array('label' => __('Vegetarian')),
	    array('label' => __('Almamater')),
	    array('label' => __('Tonti'))
	);

	// add heading with different font and bold text
	$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

	// add data
	foreach ($data as $d) {
	    $this->PhpExcel->addTableRow(array(
	        $d['Peserta']['nim'],
	        $d['Peserta']['nama'],
	        $d['Prodi']['nama_prodi'],
	        $d['Kelompok']['nama_kelompok'],
	        $d['Peserta']['jenis_kelamin'],
	        $d['Peserta']['tempat_lahir'],
	        $d['Peserta']['tanggal_lahir'],
	        $d['Peserta']['golongan_darah'],
	        $d['Peserta']['agama'],
	        $d['Peserta']['nomor_telpon'],
	        $d['Peserta']['nomor_orangtua'],
	        $d['Peserta']['alamat_asal'],
	        $d['Peserta']['alamat_jogja'],
	        $d['Peserta']['asal_sekolah'],
	        $d['Peserta']['kota_asal_sekolah'],
	        $d['Peserta']['alergi'],
	        $d['Peserta']['alergi_makanan'],
	        $d['Peserta']['penyakit_diderita'],
	        $d['Peserta']['vegetarian'],
	        $d['Peserta']['ukuran_almamater'],
	        $d['Peserta']['tonti']
	    ));
	}

	// close table and output
	$this->PhpExcel->addTableFooter()
    ->output();
?>