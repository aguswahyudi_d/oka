<style type="text/css">
    #tombol{
        float: right;
        margin: 0px 0px 10px 0px;
    }

    .header{
        margin-top: -10px;
        margin-bottom: 0px;
    }

    #tomboltambah{
        margin-right: 10px;
    }

    hr{
        margin-top: 0px;
        margin-bottom: 10px;
    }

    #total{
        float:left;
        margin-top: 15px;
        font-weight: bold;
    }

    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }

    .keterangan{
        font-style: italic;
        font-size: 13pt;
        clear:both;
        text-align: center;
        padding-top: 40px;
    }

    nav{
        text-align: center;
    }
    
</style>
<table class="header">
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar Peserta OKA 2015</h2></td>
        <td style="border-bottom:none;">
            <?php echo $this->Form->create('Peserta',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('Peserta.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('Peserta.dasar',array('label' => false,'class'=>'form-control','options'=>array('Peserta.nama'=>'Nama Peserta','Prodi.nama_prodi'=>'Program Studi'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;&nbsp;Cari',array('class'=>'btn btn-default')); ?>
                <?php echo $this->Form->end();?>
            </td>
        </td>
    </tr>
</table>
<hr/>

<div id="total">
    <?php 
        $jumlah = $this->requestAction('/Pesertas/hitungAnggota/'); 
        echo 'Total peserta (L/P) : '.$jumlah['laki-laki'].' / '.$jumlah['perempuan']; 
    ?>
</div>

<div id="tombol">
    <?php 
        echo $this->Html->link($this->Html->tag('span','', array('class' => 'glyphicon glyphicon-plus','aria-hidden'=>'true')).
                                 '&nbsp;Tambah peserta', array('controller'=>'Pesertas', 'action' => 'add'),
                                                  array('class' => 'btn btn-primary', 'id'=>'tomboltambah','target'=>'_blank','escape'=>false)
                                );
        if(!empty($pesertas)){
            echo $this->Html->link($this->Html->tag('span','', array('class' => 'glyphicon glyphicon-save','aria-hidden'=>'true')).
                                 '&nbsp;Download .xls', array('controller'=>'Pesertas', 'action' => 'download'),
                                                  array('class' => 'btn btn-primary','target'=>'_blank','escape'=>false)
                                );
        }
        else{
            echo $this->Html->link($this->Html->tag('span','', array('class' => 'glyphicon glyphicon-save','aria-hidden'=>'true')).
                                 '&nbsp;Download .xls', array('controller'=>'Pesertas', 'action' => 'download'),
                                                  array('class' => 'btn btn-primary','target'=>'_blank','escape'=>false,'disabled'=>'disabled')
                                );
        }
    ?>
</div>

<?php
    if(!empty($pesertas)){
?>
<table class="table table-bordered table-hover table-condensed">
    <tr>
        <th><?php echo $this->Paginator->sort('id','ID'); ?></th>
        <th><?php echo $this->Paginator->sort('nama','Nama'); ?></th>
        <th><?php echo $this->Paginator->sort('jenis_kelamin','Jenis Kelamin'); ?></th>
        <th><?php echo $this->Paginator->sort('prodi','Prodi'); ?></th>
        <th><?php echo $this->Paginator->sort('nim', 'NIM'); ?></th>
        <th><?php echo $this->Paginator->sort('created', 'Tanggal Pendaftaran')?></th>
        <th>Actions</th>
    </tr>
    <?php foreach ($pesertas as $peserta): ?>
    <tr>
        <td><?php echo $peserta['Peserta']['id']; ?></td>        
        <td><?php echo $this->Html->link($peserta['Peserta']['nama'], 
                                         array(
                                             'controller' => 'Pesertas', 
                                             'action' => 'detail', 
                                             $peserta['Peserta']['id']
                                         )
                                        ); 
            ?>
        </td>
        <td><?php echo $peserta['Peserta']['jenis_kelamin']; ?></td>
        <td><?php echo $peserta['Prodi']['nama_prodi']; ?></td>
        <td><?php echo $peserta['Peserta']['nim'] ?></td>
        <td><?php echo $peserta['Peserta']['created'] ?></td>
        <td>
            <?php 
                echo $this->Html->link('Edit', 
                    array('controller'=>'Pesertas','action'=>'edit',$peserta['Peserta']['id']),
                    array('class'=>'btn btn-default')
                    );
                echo $this->Form->postLink('Delete', 
                    array('controller'=>'Pesertas','action'=>'delete',$peserta['Peserta']['id']),
                    array('confirm' => 'Apakah anda yakin akan menghapus data ini?','class'=>'btn btn-default')
                    );
            ?>
        </td>
    </tr>
    <?php 
        endforeach;
        unset($pesertas);
    ?>
</table>

<nav>
    <ul class="pagination">
        <?php
            echo $this->Paginator->first(__('First'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            if($this->Paginator->hasPrev()){
                echo $this->Paginator->prev(__('<<'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            }
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            if($this->Paginator->hasNext()){
               echo $this->Paginator->next(__('>>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')); 
            }  
            echo $this->Paginator->last(__('Last'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));              
        ?>
    </ul>
 </nav>

<?php
    }
    else{
        ?>
        <div class="keterangan">
            <?php echo "-- Data peserta belum ada --"; ?>
        </div>
    <?php
    }
?>


